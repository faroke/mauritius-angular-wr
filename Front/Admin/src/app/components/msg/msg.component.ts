import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global/global.service";


interface Imessage{
  _id: string,
  prenom: string,
  nom: string,
  objet: string,
  message: string,
  __v: 0
}
@Component({
  selector: 'app-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.css']
})
export class MsgComponent implements OnInit {
  displayedColumns: string[] = ['date', 'email', 'objet', 'action'];
  dataSource: any;
  constructor(private global: GlobalService) { }

  ngOnInit(): void {
    this.global.index('msg').subscribe((messages) => {
      this.dataSource = messages;
    })
  }

  deleteMsg(id: string){
    this.global.delete('msg/' + id).subscribe(messages => {
      this.ngOnInit();
    })
  }

}
