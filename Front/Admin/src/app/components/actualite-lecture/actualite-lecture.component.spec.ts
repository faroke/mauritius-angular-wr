import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualiteLectureComponent } from './actualite-lecture.component';

describe('ActualiteLectureComponent', () => {
  let component: ActualiteLectureComponent;
  let fixture: ComponentFixture<ActualiteLectureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualiteLectureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualiteLectureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
