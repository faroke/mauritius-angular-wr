import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global/global.service";
@Component({
  selector: 'app-msg-lecture',
  templateUrl: './msg-lecture.component.html',
  styleUrls: ['./msg-lecture.component.css']
})
export class MsgLectureComponent implements OnInit {
  public msg: any;
  constructor(private global: GlobalService) { }
  ngOnInit(): void {
    this.global.index('msg' + location.pathname.slice(4)).subscribe((messages) => {
      this.msg = messages;
    })
  }
}
