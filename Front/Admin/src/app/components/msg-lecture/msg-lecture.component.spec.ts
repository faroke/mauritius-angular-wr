import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsgLectureComponent } from './msg-lecture.component';

describe('MsgLectureComponent', () => {
  let component: MsgLectureComponent;
  let fixture: ComponentFixture<MsgLectureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MsgLectureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MsgLectureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
