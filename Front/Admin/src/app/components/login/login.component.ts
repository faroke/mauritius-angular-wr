import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global/global.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private global: GlobalService) { }

  ngOnInit(): void {
  }
  onSubmit(form: any){
    this.global.login({
      email: form.controls.email.value,
      password: form.controls.password.value
    }).subscribe((data: {message: string, token: string, user: {}}) => {
      localStorage.setItem('token', data.token);
      this.ngOnInit();
    });
  }
}
