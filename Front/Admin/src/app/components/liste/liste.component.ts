import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global/global.service";

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
  displayCounter: number = 0;
  displayedColumnsArticle: string[] = ['title', 'filtres', 'date', 'action'];
  displayedColumnsActualite: string[] = ['title', 'date', 'description', 'action'];
  dataSourceArticle: any;
  dataSourceActualite: any;
  constructor(private global: GlobalService) { }

  ngOnInit(): void {
    this.global.index('article').subscribe(message => {
      this.dataSourceArticle = message;
    })
    this.global.index('actualite').subscribe(message => {
      this.dataSourceActualite = message;
    })
  }
  deleteArticle(id: string){
    this.global.delete('article' + id).subscribe(message => {
      this.ngOnInit();
    })
  }
  deleteActualite(id: string){
    this.global.delete('actualite' + id).subscribe(message => {
      this.ngOnInit();
    })
  }
}
