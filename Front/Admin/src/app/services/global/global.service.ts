import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
interface login {
  email: string,
  password: string
}
@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public url: string = 'http://188.166.127.82';
  httpHeaders = new HttpHeaders({
    'Content-Type' : 'application/json',
    'Authorization' : "Bearer " + localStorage.getItem('token')
  })
  constructor(private http: HttpClient) { }
  public login(code: login): Observable<any>{
    return this.http.post(this.url + '/auth/login', code);
  }
  public logout(){
    localStorage.removeItem('token');
  }
  public index(lien: string){
    return this.http.get(this.url + '/' + lien, {headers: this.httpHeaders});
  }
  public update(lien: string, content: {}){
    return this.http.put(this.url + '/' + lien, content, {headers: this.httpHeaders});
  }
  public create(lien: string, content: {}){
    return this.http.post(this.url + '/' + lien, content, {headers: this.httpHeaders});
  }
  public delete(lien: string){
    return this.http.delete(this.url + '/' + lien,  {headers: this.httpHeaders});
  }
}
