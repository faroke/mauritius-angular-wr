import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SidebarComponent} from "./components/sidebar/sidebar.component";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { ListeComponent } from './components/liste/liste.component';
import { AddcontentComponent } from './components/addcontent/addcontent.component';
import { FiltresComponent } from './components/filtres/filtres.component';
import { MsgComponent } from './components/msg/msg.component';
import { MdpComponent } from './components/mdp/mdp.component';
import {RouterModule, Routes } from '@angular/router';
import { MsgLectureComponent } from './components/msg-lecture/msg-lecture.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from "@angular/material/sidenav";
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatTableModule} from "@angular/material/table";
import { ArticleLectureComponent } from './components/article-lecture/article-lecture.component';
import { ActualiteLectureComponent } from './components/actualite-lecture/actualite-lecture.component';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatChipsModule} from "@angular/material/chips";

const routes: Routes = [
  {path: '', component: ListeComponent},
  {path: 'article/:id', component: ArticleLectureComponent},
  {path: 'actualite/:id', component: ActualiteLectureComponent},
  {path: 'addcontent', component: AddcontentComponent},
  {path: 'filtres', component: FiltresComponent},
  {path: 'msg', component: MsgComponent},
  {path: 'msg/:id', component: MsgLectureComponent},
  {path: 'mdp', component: MdpComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    ListeComponent,
    AddcontentComponent,
    FiltresComponent,
    MsgComponent,
    MdpComponent,
    MsgLectureComponent,
    ArticleLectureComponent,
    ActualiteLectureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
