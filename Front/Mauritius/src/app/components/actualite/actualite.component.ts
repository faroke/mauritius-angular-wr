import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global/global.service";
import {ActivatedRoute} from "@angular/router";
interface IActualite{
  filtre: string[],
  _id: number,
  titre: string,
  sousTitre: string,
  description: string,
  contenu: string,
  date: string,
  __v: number
}
@Component({
  selector: 'app-actualite',
  templateUrl: './actualite.component.html',
  styleUrls: ['./actualite.component.css']
})
export class ActualiteComponent implements OnInit{
  public actus: IActualite[];
  constructor(private global: GlobalService) { }

  ngOnInit(): void {
    this.global.index('/actualite').subscribe((actus: IActualite[]) => {
      this.actus = actus;
    })
  }
}
