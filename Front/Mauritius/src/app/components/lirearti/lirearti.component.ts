import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GlobalService} from "../../services/global/global.service";
import {Title} from "@angular/platform-browser";

interface IArticle{
  filtre: string[],
  _id: number,
  titre: string,
  sousTitre: string,
  description: string,
  contenu: string,
  date: string,
  __v: number
}

@Component({
  selector: 'app-lirearti',
  templateUrl: './lirearti.component.html',
  styleUrls: ['./lirearti.component.css']
})
export class LireartiComponent implements OnInit {
  public article: IArticle;

  constructor(private global: GlobalService,
              private route: ActivatedRoute,
              private titleService: Title) { }

  ngOnInit(): void {
    this.global.byId('/article', this.route.snapshot.params.id).subscribe((article: IArticle) => {
      this.article = article;
      this.titleService.setTitle(this.article.titre);
      console.log(article)
    })
  }

}
