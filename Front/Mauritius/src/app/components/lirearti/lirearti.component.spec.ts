import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LireartiComponent } from './lirearti.component';

describe('LireartiComponent', () => {
  let component: LireartiComponent;
  let fixture: ComponentFixture<LireartiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LireartiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LireartiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
