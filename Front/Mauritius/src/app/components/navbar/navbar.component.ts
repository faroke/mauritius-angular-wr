import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  title: string = "Mauritius";
  public links: {title: string, url: string}[] =
    [
      {title: 'Acceuil', url: '/'},
      {title: 'Que faire ?', url: '/article'},
      {title: 'Actualités', url: '/actualite'},
      ];

  constructor(private titleService: Title) {}

  ngOnInit(): void {
  }
  main(){
    switch (location.pathname){
      case "/": {
        this.titleService.setTitle('Mauritius');
        return {titre : "Welcome to Paradise", bgSize : '640px', textSize: '200px'}
      }
      case "/article": {
        this.titleService.setTitle('Que faire ?');
        return {titre : "Que faire ?", bgSize : '339px', textSize: '125px' }
      }
      case "/actualite": {
        this.titleService.setTitle('Actualités');
        return {titre : "Actualités", bgSize : '339px', textSize: '125px' }
      }
      default: {
        return {titre : this.titleService.getTitle(), bgSize : '339px', textSize: '125px' }
      }
    }
  }
}
