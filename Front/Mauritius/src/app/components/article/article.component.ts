import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global/global.service";
interface IArticle{
  filtre: string[],
  _id: number,
  titre: string,
  sousTitre: string,
  description: string,
  contenu: string,
  date: string,
  __v: number
}
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  public articles: IArticle[];
  constructor(private global: GlobalService) {
  }
  ngOnInit(): void {
    this.global.index('/article').subscribe((articles: IArticle[]) => {
      this.articles = articles;
    })
  }
}
