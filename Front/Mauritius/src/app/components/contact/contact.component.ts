import {Component, OnInit} from '@angular/core';
import {GlobalService} from "../../services/global/global.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  objet = [
    'question', 'article'
  ]
  constructor(private global: GlobalService) { }
  ngOnInit(): void {

  }
  onSubmit(form){
    this.global.msg('/msg', {
      prenom: form.controls.prenom.value,
      nom: form.controls.nom.value,
      email: form.controls.email.value,
      objet: form.controls.objet.value,
      message: form.controls.message.value
    }).subscribe((data) => {
      console.log(data)
    });
  }
}
