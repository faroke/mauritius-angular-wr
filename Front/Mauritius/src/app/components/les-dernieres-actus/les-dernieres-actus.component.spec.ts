import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LesDernieresActusComponent } from './les-dernieres-actus.component';

describe('LesDernieresActusComponent', () => {
  let component: LesDernieresActusComponent;
  let fixture: ComponentFixture<LesDernieresActusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LesDernieresActusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LesDernieresActusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
