import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
@Component({
  selector: 'app-les-dernieres-actus',
  templateUrl: './les-dernieres-actus.component.html',
  styleUrls: ['./les-dernieres-actus.component.css']
})
export class LesDernieresActusComponent implements OnInit {
  public actus: {filtre: string[], _id: number, titre: string, sousTitre: string, description: string, contenu: string, date: string, __v: number }[];
  public sub;
  constructor(private http: HttpClient) {
  }
  ngOnInit(): void {
    this.sub = this.http.get('http://188.166.127.82/actualite/third').subscribe((response: {filtre: string[], _id: number, titre: string, sousTitre: string, description: string, contenu: string, date: string, __v: number }[]) => {
      this.actus = response;
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
