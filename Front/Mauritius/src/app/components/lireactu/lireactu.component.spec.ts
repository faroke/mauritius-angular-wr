import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LireactuComponent } from './lireactu.component';

describe('LireactuComponent', () => {
  let component: LireactuComponent;
  let fixture: ComponentFixture<LireactuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LireactuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LireactuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
