import {Component, OnInit} from '@angular/core';
import {GlobalService} from "../../services/global/global.service";
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
interface IActualite{
  filtre: string[],
  _id: number,
  titre: string,
  sousTitre: string,
  description: string,
  contenu: string,
  date: string,
  __v: number
}
@Component({
  selector: 'app-lireactu',
  templateUrl: './lireactu.component.html',
  styleUrls: ['./lireactu.component.css']
})
export class LireactuComponent implements OnInit {
  public actualite: IActualite;
  constructor(private global: GlobalService,
              private route: ActivatedRoute,
              private titleService: Title) { }

  ngOnInit(): void {
    this.global.byId('/actualite', this.route.snapshot.params.id).subscribe((actualite: IActualite) => {
      this.actualite = actualite;
      this.titleService.setTitle(this.actualite.titre);
      console.log(actualite)
    })
  }
}
