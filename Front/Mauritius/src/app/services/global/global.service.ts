import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public url: string = 'http://188.166.127.82';
  constructor(private http: HttpClient) { }
  public index(lien: string): Observable<any>{
    return this.http.get(this.url + lien);
  }
  public byId(lien:string, id:string): Observable<any>{
    return this.http.get(this.url + lien + '/' + id);
  }
  public msg(lien:string, message): Observable<any>{
    return this.http.post(this.url + lien, message);
  }
}
