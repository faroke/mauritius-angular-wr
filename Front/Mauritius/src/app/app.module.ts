import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NavbarComponent} from "./components/navbar/navbar.component";
import { HomepageComponent } from './components/homepage/homepage.component';
import { ArticleComponent } from './components/article/article.component';
import { ActualiteComponent } from './components/actualite/actualite.component';
import { LesDernieresActusComponent } from './components/les-dernieres-actus/les-dernieres-actus.component';
import { ContactComponent } from './components/contact/contact.component';
import {FormsModule} from "@angular/forms";
import { LireactuComponent } from './components/lireactu/lireactu.component';
import { LireartiComponent } from './components/lirearti/lirearti.component';




const routes: Routes = [
  {path: 'article', component: ArticleComponent},
  {path: 'actualite', component: ActualiteComponent},
  {path: 'actualite/:id', component: LireactuComponent},
  {path: 'article/:id', component: LireartiComponent},
  {path : '', component: HomepageComponent}

]


@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        HomepageComponent,
        ArticleComponent,
        ActualiteComponent,
        LesDernieresActusComponent,
        ContactComponent,
        LireactuComponent,
        LireartiComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
