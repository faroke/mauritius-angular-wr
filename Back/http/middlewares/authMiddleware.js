const jwt = require('jsonwebtoken');
const model = require('../../models/authModel');
const authMiddleware = function (req, res, next){
    console.log(req.headers.authorization);
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, 'MGyhmTK6s3$ytdSgK2a+yUfdS!ZQCAxRb*6Bvf?wEN9Lp!w^b5!w-Ch##dsfH6RLxz7%k2RuWcqZqrV3^P79jh^ca?rJcHtFqEVkmvaGDmg#w794FLe!m4BX7wD!!vTAYmjbjgFY?Ym4$CSTa4ScyeQRtu@SGzrC+Ub3g68u!*Mm!MvKxq^N*2%By#7zZfG7Kjt%Ntve_UK9GKg2Nju9N@2HK&km-_b2!?DTagB@aTy785CQcZR6js6?h^hv$S9h')
    model.findById(decoded.sub).exec((err, user) => {
        if (!!err){
            res.status(401).send({message : "Vous n'êtes pas connecté !"});
        }
        req.user = user;
        next();
    });
}
module.exports = authMiddleware;
