const model = require("../../models/msgModel");
class msgController {
    index(req, res) {
        model.find((err, msg) => {
            res.send(msg)
        });
    }
    post(req, res){
        const msg = new model(req.body);
        msg.save().then(() => res.send({message: 'msg enregistré'})).catch(err => res.status(500).send({erreur: err}));
    }
    getById(req, res){
        model.findById(req.params.id, (err, getmodel) => {
            res.send(getmodel);
        })
    }
    delete(req, res){
        model.findByIdAndDelete(req.params.id, (err, a) => {
            if (!!err){
                res.status(404).send({message: "Cannot find smg"})
            }
            res.send({message: "msg deleted successfully", a});
        })
    }
}
module.exports = new msgController();
