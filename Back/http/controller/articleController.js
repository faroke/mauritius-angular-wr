const model = require("../../models/articleModel");
class articleController {
    index(req, res) {
        model.find((err, article) => {
            res.send(article)
        });
    }
    post(req, res){
        const article = new model(req.body);
        article.save().then(() => res.send({message: 'Article enregistré'})).catch(err => res.status(500).send({erreur: err}));
    }
    getById(req, res){
        model.findById(req.params.id, (err, getmodel) => {
            res.send(getmodel);
        })
    }
    put(req, res){
        model.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, a) => {
            if(!!err) {
                res.status(404).send({message: 'Article not found!'});
            }
            res.send({message: 'Article updated successfully', a})
        })
    }
    delete(req, res){
        model.findByIdAndDelete(req.params.id, (err, a) => {
            if (!!err){
                res.status(404).send({message: "Cannot find Article"})
            }
            res.send({message: "Article deleted successfully", a});
        })
    }
}
module.exports = new articleController();
