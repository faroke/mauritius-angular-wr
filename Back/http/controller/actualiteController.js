const model = require("../../models/actualiteModel");
class actualiteController {
    index(req, res) {
        model.find((err, actualite) => {
            res.send(actualite)
        });
    }
    post(req, res){
        const actu = new model(req.body);
        actu.save().then(() => res.send({message: 'Actualitée enregistré'})).catch(err => res.status(500).send({erreur: err}));
    }
    getById(req, res){
        model.findById(req.params.id, (err, getmodel) => {
            res.send(getmodel);
        })
    }
    put(req, res){
        model.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, a) => {
            if(!!err) {
                res.status(404).send({message: 'actualite not found!'});
            }
            res.send({message: 'Actualite updated successfully', a})
        })
    }
    delete(req, res){
        model.findByIdAndDelete(req.params.id, (err, a) => {
            if (!!err){
                res.status(404).send({message: "Cannot find Actualite"})
            }
            res.send({message: "Actualite deleted successfully", a});
        })
    }
    third(req, res) {
        model.find((err, actualite) => {
            res.send(actualite.slice(actualite.length - 3, actualite.length))
        });
    }
}
module.exports = new actualiteController();
