const model = require("../../models/authModel");
const jwt = require("jsonwebtoken");

class authController {
    login(req, res) {
        model.findOne({email: req.body.email, password: req.body.password})
            .exec((err, user) => {
                if (!!err) {
                    res.status(500).send({message: "Une erreur est survenue lors de votre tentative de connexion !"});
                }
                if (!!user) {
                    res.send({
                        message: 'Authentification réussie',
                        token: jwt.sign({email : user.email}, 'MGyhmTK6s3$ytdSgK2a+yUfdS!ZQCAxRb*6Bvf?wEN9Lp!w^b5!w-Ch##dsfH6RLxz7%k2RuWcqZqrV3^P79jh^ca?rJcHtFqEVkmvaGDmg#w794FLe!m4BX7wD!!vTAYmjbjgFY?Ym4$CSTa4ScyeQRtu@SGzrC+Ub3g68u!*Mm!MvKxq^N*2%By#7zZfG7Kjt%Ntve_UK9GKg2Nju9N@2HK&km-_b2!?DTagB@aTy785CQcZR6js6?h^hv$S9h', {
                            expiresIn : 3600
                        }),
                        user
                    })
                }
            });
    }
    register(req, res){
        const account = new model(req.body);
        account.save().then(() => res.send({message: 'account enregistré'})).catch(err => res.status(500).send({erreur: err}));
    }
}

module.exports = new authController();
