const model = require("../../models/filtreModel");
class filtreController {
    index(req, res) {
        model.find((err, filtres) => {
            res.send(filtres)
        });
    }
    post(req, res){
        const filtre = new model(req.body);
        filtre.save().then(() => res.send({message: 'filtre enregistré'})).catch(err => res.status(500).send({erreur: err}));
    }
    delete(req, res){
        model.findByIdAndDelete(req.params.id, (err, a) => {
            if (!!err){
                res.status(404).send({message: "Cannot find filtre"})
            }
            res.send({message: "filtre deleted successfully", a});
        })
    }
}
module.exports = new filtreController();
