const mongoose = require('mongoose');
const HOST = '127.0.0.1';
const bdd = 'mauritius'
// MONGOOSE CONFIG
mongoose.connect(`mongodb://${HOST}/${bdd}`, {useNewUrlParser: true})
    .then(() => console.log('Successfully connected to dtb'))
    .catch((err) => console.error(err))

module.exports = mongoose.connection;
