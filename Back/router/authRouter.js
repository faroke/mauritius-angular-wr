const express = require('express');
const Controller = require('../http/controller/authController');
const Middleware = require('../http/middlewares/authMiddleware');
const router = express.Router();
const prefix = '/auth';
router.post('/login', Controller.login);
router.post('/logout', Middleware, Controller.logout);
router.post('/register', Middleware, Controller.register);

module.exports = {prefix, router};
