const express = require('express');
const router = express.Router();
const prefix = '/filtre';
const Controller = require('../http/controller/filtreController');
const Middleware = require('../http/middlewares/authMiddleware');

router.get( '/', Controller.index);
router.post( '/', Middleware, Controller.post);
router.delete('/:id', Middleware, Controller.delete);
module.exports = {router, prefix};
