const express = require('express');
const router = express.Router();
const prefix = '/msg';
const Controller = require('../http/controller/msgController');
const Middleware = require('../http/middlewares/authMiddleware');

router.get( '/', Middleware, Controller.index);
router.get('/:id', Middleware, Controller.getById);
router.post( '/', Controller.post);
router.delete('/:id', Middleware, Controller.delete);
module.exports = {router, prefix};
