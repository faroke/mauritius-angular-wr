const express = require('express');
const router = express.Router();
const prefix = '/actualite';
const Controller = require('../http/controller/actualiteController');
const Middleware = require('../http/middlewares/authMiddleware');

router.get( '/', Controller.index);
router.get( '/third', Controller.third);
router.get('/:id', Controller.getById);
router.post( '/', Middleware, Controller.post);
router.put('/:id', Middleware, Controller.put);
router.delete('/:id', Middleware, Controller.delete);
module.exports = {router, prefix};
