const express = require('express');
const server = express();
const cors = require('cors');
const PORT = 8080;
const HOST = '127.0.0.1';
// CORS CONFIG
server.use(cors({
    origin: '*',
    optionsSuccessStatus: 200
}))
// EXPRESS JSON
server.use(express.json());

const authRouter = require('./router/authRouter');
const actualiteRouter = require('./router/actualiteRouter');
const articleRouter = require('./router/articleRouter');
const filtreRouter = require('./router/filtreRouter');
const msgRouter = require('./router/msgRouter');

server.use(authRouter.prefix, authRouter.router);
server.use(actualiteRouter.prefix, actualiteRouter.router);
server.use(articleRouter.prefix, articleRouter.router);
server.use(filtreRouter.prefix, filtreRouter.router);
server.use(msgRouter.prefix, msgRouter.router);

// Starting server
server.listen(PORT, HOST, function () {
    console.log(`Server running on http://${HOST}:${PORT}`);
})
