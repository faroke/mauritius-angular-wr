const mongoose = require('mongoose');

// Créer un schéma / DOCUMENT
const ActualiteSchema = new mongoose.Schema({
    titre: {
        type: String,
        required: true
    },
    sousTitre: String,
    description: String,
    contenu: String,
    date: Date,
})

// Créer un model et l'exporter
module.exports = mongoose.model('Actualite', ActualiteSchema);

