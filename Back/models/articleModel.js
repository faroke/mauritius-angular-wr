const mongoose = require('mongoose');

// Créer un schéma / DOCUMENT
const ArticleSchema = new mongoose.Schema({
    titre: {
        type: String,
        required: true
    },
    sousTitre: String,
    description: String,
    contenu: String,
    filtre : [String],
    date: Date,
})

// Créer un model et l'exporter
module.exports = mongoose.model('Article', ArticleSchema);

