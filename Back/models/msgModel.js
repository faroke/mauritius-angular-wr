const mongoose = require('mongoose');

// Créer un schéma / DOCUMENT
const msgSchema = new mongoose.Schema({
    prenom: String,
    nom: String,
    mail: String,
    objet: String,
    message: String
})

// Créer un model et l'exporter
module.exports = mongoose.model('msg', msgSchema);

