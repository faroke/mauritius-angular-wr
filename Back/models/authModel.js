const mongoose = require('mongoose');

// Créer un schéma / DOCUMENT
const AuthSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})
// Créer un model et l'exporter
module.exports = mongoose.model('Auth', AuthSchema);

